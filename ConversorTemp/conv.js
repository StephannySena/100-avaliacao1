//ERRO 1: Referência no script da página html estava no local incorreto
var btnConverter = document.getElementById("btnConverter")

btnConverter.onclick = function () {
    let tempEntrada = Number(document.getElementById("idTemp").value)
    let unidadeEntrada = document.querySelector("#idUnidadeOrigem").value //ERRO 2: faltava o "#" antes do id      
    let unidadeConvesao = document.querySelector("#idUnidadeConvertido").value

    //Conversao da unidade de entrada para Celsius 
    let tempCesius

    switch (unidadeEntrada) {
        case "C":
            tempCesius = tempEntrada
            break;

        case "F":
            tempCesius = (tempEntrada * 5 - 160) / 9
            break; //ERRO 3: faltava um break; aqui

        case "K":
            tempCesius = tempEntrada - 273
            break; 

        default:
            break;
    }

    //Conversao de celsius para a unidade de saida 
    let tempConvertido
    switch (unidadeConvesao) {
        case "C":
            tempConvertido = tempCesius
            break;
        case "F":
            tempConvertido = (9 * tempCesius + 160) / 5
            break;
        case "K":
            tempConvertido = tempCesius + 273
            break;
        default:
            break;
    }

    document.getElementById("idTempConvertido").value = tempConvertido


}
