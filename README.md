

8. Na sua interpretação:
a. Qual foi o nível de dificuldade da implementação da questão do bloco A?
[ ] Muito Fácil
[ x ] Fácil (para o Exercício 2)
[ x ] Médio (para o Exercício 5 - não consegui terminar a tempo)
[ ] Difícil
[ ] Muito Difícil

b. Qual foi o nível de dificuldade para encontrar os erros do programa da questão 7?
[ ] Muito Fácil
[ ] Fácil
[ x ] Médio
[ ] Difícil
[ ] Muito Difícil